
#include "template_robot.hpp"

// do some other things that the robot might need that don't fit in the main

// leave this file here even if you don't do anything with it,
// since this will force inclusion of the robot header in the lib

/*
 * Ex.
 *
 * make decisions based on camera or lidar position
 * set up the play area for SLAM
 * specify other functions or whatever
 */
