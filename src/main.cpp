#include <signal.h>

#include <chrono>
#include <rip/exception_base.hpp>
#include <rip/logger.hpp>
#include <thread>

#include "template_robot.hpp"

#define LOG_DEBUG rip::Logger::debug
#define LOG_INFO rip::Logger::info
#define LOG_ERR rip::Logger::error

std::shared_ptr< rip::Robot > robot = nullptr;

void signalHandler(int signum)
{
    LOG_DEBUG("Caught Ctrl-C. Stoping the robot.");
    robot->stop();
}

int main(int argc, char** argv)
{
    bool diag = argc > 1 && std::string(argv[1]) == "--diag";
    robot     = std::shared_ptr< rip::Robot >(new template_robot::TemplateRobot("TemplateRobot", "config.json"));
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    signal(SIGINT, signalHandler);
    try
    {
        robot->load();
        if(diag)
        {
            LOG_INFO("Diag mode starts...");
            std::this_thread::sleep_for(std::chrono::milliseconds(500));

            // XXX do your stuff here

            LOG_INFO("Diag exited.");
        }
        else
        {  // robot not in diag mode
            LOG_INFO("robot->start()");
            robot->start();
        }
    }
    catch(rip::ExceptionBase e)
    {
        LOG_ERR("Caught an exception from RIP.");
        LOG_ERR(e.what());
        robot->stop();
        throw e;
        return 1;
    }

    LOG_INFO("End of program. Stopping robot...");

    robot->stop();

    LOG_INFO("Robot stopped.");

    return 0;
}
