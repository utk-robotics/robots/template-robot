#ifndef TEMPLATE_ROBOT_HPP
#define TEMPLATE_ROBOT_HPP

#include <rip/components/template_component.hpp>
#include <rip/robot.hpp>
#include <rip/routines/template_routine.hpp>

namespace template_robot
{
    class TemplateRobot : public rip::Robot
    {
    public:
        TemplateRobot(const std::string& name, const std::string& config_path);
    };

    TemplateRobot::TemplateRobot(const std::string& name, const std::string& config_path)
        : Robot(name)
    {}

    using TemplateComponent = rip::components::TemplateComponent;
    using TemplateRoutine   = rip::routines::TemplateRoutine;

}  // namespace template_robot

#endif  // TEMPLATE_ROBOT_HPP
